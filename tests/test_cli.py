#!/usr/bin/env python3
import json
import sys
from unittest import mock

import pytest

from tests.helpers_tests import run_jsonflattifier_cli

CLI_TEST_CASES_DATA_FILE = "./test-cases/cli-test-cases-data.json"

with open(CLI_TEST_CASES_DATA_FILE) as cli_test_cases_data_json_file:
    f = json.load(cli_test_cases_data_json_file)

flattify_normalised_path_test_cases_data = [
    (i["input_file"], True, i["flattify/s_normalised_path"]) for i in f
]

flattify_test_cases_data = []
flattify_test_cases_data.extend(
    [(i["input_file"], False, i["flattify/s_non_normalised_path"]) for i in f]
)
flattify_test_cases_data.extend(
    [(i["input_file"], True, i["flattify/s_normalised_path"]) for i in f]
)


# flattify "./data/inputs-repository/0-in.json" --path --json [--jsonpath-keys] --no-table
@pytest.mark.parametrize(
    "input_file,normalised_path,output_file", flattify_test_cases_data
)
def test_flattify(capsys, input_file, normalised_path, output_file):
    # Given
    with open(output_file) as output_json:
        expected_output_test_data_json = json.load(output_json)
    print(json.dumps(expected_output_test_data_json, indent=2))
    out, err = capsys.readouterr()
    # When
    # TODO: Optimise if-else;
    #    jsonpath_keys = "--jsonpath-keys" if normalised_path else ""
    #  does not work, error message:
    #    'jsonflattifier: error: unrecognized arguments: \n'
    if normalised_path:
        run_jsonflattifier_cli(
            [
                "flattify",
                input_file,
                "--path",
                "--json",
                "--jsonpath-keys",
                "--no-table",
            ]
        )
    else:
        run_jsonflattifier_cli(
            ["flattify", input_file, "--path", "--json", "--no-table"]
        )
    # Then
    captured = capsys.readouterr()
    assert captured.out == out


flattifys_test_cases_data = []
flattifys_test_cases_data.extend(
    [(i["input_file"], False, i["flattify/s_non_normalised_path"]) for i in f]
)
flattifys_test_cases_data.extend(
    [(i["input_file"], True, i["flattify/s_normalised_path"]) for i in f]
)


# flattify "{...}" --json [--jsonpath-keys] --no-table
@pytest.mark.parametrize(
    "input_file,normalised_path,output_file", flattify_test_cases_data
)
def test_flattifys(capsys, input_file, normalised_path, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()
    with open(output_file) as output_json:
        expected_output_test_data_json = json.load(output_json)
    print(json.dumps(expected_output_test_data_json, indent=2))
    out, err = capsys.readouterr()
    # When
    # TODO: Optimise if-else;
    #    jsonpath_keys = "--jsonpath-keys" if normalised_path else ""
    #  does not work, error message:
    #    'jsonflattifier: error: unrecognized arguments: \n'
    if normalised_path:
        run_jsonflattifier_cli(
            [
                "flattify",
                given_input_test_data_s,
                "--json",
                "--jsonpath-keys",
                "--no-table",
            ]
        )
    else:
        run_jsonflattifier_cli(
            ["flattify", given_input_test_data_s, "--json", "--no-table"]
        )
    # Then
    captured = capsys.readouterr()
    assert captured.out == out


flatjson_to_csv_test_cases_data = []
flatjson_to_csv_test_cases_data.extend(
    [(i["input_file"], i["flatjson_to_csv"]) for i in f]
)


# flattify "{...}" --csv --no-table
@pytest.mark.parametrize("input_file,output_file", flatjson_to_csv_test_cases_data)
def test_flatjson_to_csv(capsys, input_file, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()

    with open(output_file, "r") as output_csv:
        expected_output = output_csv.read()
    # When
    run_jsonflattifier_cli(
        ["flattify", given_input_test_data_s, "--csv", "--no-table",]
    )
    # Then
    captured = capsys.readouterr()
    assert captured.out.strip() == expected_output.strip()


flatjson_to_print_test_cases_data = []
flatjson_to_print_test_cases_data.extend(
    [(i["input_file"], i["flatjson_to_print"]) for i in f]
)


# flattify "{...}"
@pytest.mark.parametrize("input_file,output_file", flatjson_to_print_test_cases_data)
def test_flatjson_to_print(capsys, input_file, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()

    with open(output_file, "r") as output_csv:
        expected_output = output_csv.read()
    # When
    run_jsonflattifier_cli(["flattify", given_input_test_data_s])
    # Then
    captured = capsys.readouterr()
    assert captured.out.strip() == expected_output.strip()


# flattify "--help"
def test_help_text(monkeypatch, capsys):
    # When
    mock_exit = mock.Mock(side_effect=ValueError("raised in test to exit early"))
    with mock.patch.object(sys, "exit", mock_exit), pytest.raises(
        ValueError, match="raised in test to exit early"
    ):
        run_jsonflattifier_cli(["--help"])
    # Then
    captured = capsys.readouterr()
    assert "Provide the JSON Document String or a Path" in captured.out


# flattify "flattify --help"
# TODO: Works when run from the CLI, but not here...
@pytest.mark.skip(reason="Works when run from the CLI, but not here. TODO")
def test_simple_run(monkeypatch, capsys):
    # When
    run_jsonflattifier_cli(["flattify", "-h"])
    # Then
    captured = capsys.readouterr()
    assert "Provide the JSON Document String or a Path" not in captured.out
