import json

import pytest

import jsonflattifier

API_TEST_CASES_DATA_FILE = "./test-cases/api-test-cases-data.json"

with open(API_TEST_CASES_DATA_FILE) as api_test_cases_data_json_file:
    f = json.load(api_test_cases_data_json_file)

flattify_normalised_path_test_cases_data = [
    (i["input_file"], True, i["flattify/s_normalised_path"]) for i in f
]

flattify_test_cases_data = []
flattify_test_cases_data.extend(
    [(i["input_file"], False, i["flattify/s_non_normalised_path"]) for i in f]
)
flattify_test_cases_data.extend(
    [(i["input_file"], True, i["flattify/s_normalised_path"]) for i in f]
)


@pytest.mark.parametrize(
    "input_file,normalised_path,output_file", flattify_test_cases_data
)
def test_flattify(input_file, normalised_path, output_file):
    # Given
    with open(output_file) as output_json:
        expected_output_test_data_json = json.load(output_json)
    # When
    actual_output_test_data_s = jsonflattifier.flattify(
        input_file, normalised_path=bool(normalised_path),
    )
    actual_output_test_data_json = json.loads(actual_output_test_data_s)
    # Then
    assert actual_output_test_data_json == expected_output_test_data_json


flattifys_test_cases_data = []
flattifys_test_cases_data.extend(
    [(i["input_file"], False, i["flattify/s_non_normalised_path"]) for i in f]
)
flattifys_test_cases_data.extend(
    [(i["input_file"], True, i["flattify/s_normalised_path"]) for i in f]
)


@pytest.mark.parametrize(
    "input_file,normalised_path,output_file", flattify_test_cases_data
)
def test_flattifys(input_file, normalised_path, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()
    with open(output_file) as output_json:
        expected_output_test_data_json = json.load(output_json)
    # When
    actual_output_test_data_s = jsonflattifier.flattifys(
        given_input_test_data_s, normalised_path=bool(normalised_path),
    )
    actual_output_test_data_json = json.loads(actual_output_test_data_s)
    # Then
    assert actual_output_test_data_json == expected_output_test_data_json


flatjson_to_csv_test_cases_data = []
flatjson_to_csv_test_cases_data.extend(
    [(i["flattify/s_non_normalised_path"], i["flatjson_to_csv"]) for i in f]
)
flatjson_to_csv_test_cases_data.extend(
    [(i["flattify/s_normalised_path"], i["flatjson_to_csv"]) for i in f]
)


@pytest.mark.parametrize("input_file,output_file", flatjson_to_csv_test_cases_data)
def test_flatjson_to_csv(input_file, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()

    with open(output_file, "r") as output_csv:
        expected_output = output_csv.read()
    # When
    actual_output = jsonflattifier.flatjson_to_csv(given_input_test_data_s)
    # Then
    assert actual_output == expected_output


flatjson_to_transposed_list_test_cases_data = []
flatjson_to_transposed_list_test_cases_data.extend(
    [(i["flattify/s_non_normalised_path"], i["flatjson_to_transposed_list"]) for i in f]
)
flatjson_to_transposed_list_test_cases_data.extend(
    [(i["flattify/s_normalised_path"], i["flatjson_to_transposed_list"]) for i in f]
)


@pytest.mark.parametrize(
    "input_file,output_file", flatjson_to_transposed_list_test_cases_data
)
def test_flatjson_to_transposed_list(input_file, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()

    with open(output_file, "r") as output_csv:
        expected_output = output_csv.read()
    # When
    actual_output = jsonflattifier.flatjson_to_transposed_list(given_input_test_data_s)
    # Then
    assert str(actual_output) == expected_output


flatjson_to_print_test_cases_data = []
flatjson_to_print_test_cases_data.extend(
    [(i["flattify/s_non_normalised_path"], i["flatjson_to_print"]) for i in f]
)
flatjson_to_print_test_cases_data.extend(
    [(i["flattify/s_normalised_path"], i["flatjson_to_print"]) for i in f]
)


@pytest.mark.parametrize("input_file,output_file", flatjson_to_print_test_cases_data)
def test_flatjson_to_print(capfd, input_file, output_file):
    # Given
    with open(input_file, "r") as input_json:
        given_input_test_data_s = input_json.read()

    with open(output_file, "r") as output_csv:
        expected_output = output_csv.read()
    # When
    actual_output = jsonflattifier.flatjson_to_print(given_input_test_data_s)
    # Then
    out, err = capfd.readouterr()
    assert out == expected_output
