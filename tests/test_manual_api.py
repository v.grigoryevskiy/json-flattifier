import jsonflattifier


def main():
    filename = "./data/inputs-repository/0-in.json"

    flattify = jsonflattifier.flattify(filename, True)
    print(flattify)
    print("=========================")

    s = open(filename, "r").read()
    flattifys = jsonflattifier.flattifys(s)
    print(flattifys)
    print("=========================")

    flatjson_to_transposed_list = jsonflattifier.flatjson_to_transposed_list(flattify)
    print(flatjson_to_transposed_list)
    print("=========================")

    flatjson_to_csv = jsonflattifier.flatjson_to_csv(flattify)
    [print(row) for row in flatjson_to_csv.split("\r")]
    print("=========================")

    jsonflattifier.flatjson_to_print(flattify)


if __name__ == "__main__":
    main()
